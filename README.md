# ng-parse

<img src="https://gitlab.com/jefvanhoyweghen/ng-parse/raw/4d8a6742a7f55c5b3edcdeb4562daf48b7444bf6/img/ng-parse.png" alt="ng-parse" width="200px"/> 

Parse Angular typescript files using Abstract Syntax Tree (AST).

[![pipeline status](https://gitlab.com/jefvanhoyweghen/ng-parse/badges/master/pipeline.svg)](https://gitlab.com/jefvanhoyweghen/ng-parse/commits/master) [![coverage report](https://gitlab.com/jefvanhoyweghen/ng-parse/badges/master/coverage.svg)](https://gitlab.com/jefvanhoyweghen/ng-parse/commits/master)

<a href="https://www.npmjs.com/package/ng-parse" style="display:block; margin-left: auto;"><img src="https://nodei.co/npm/ng-parse.png?compact=true"></a>

## Usage
Install with npm

```
npm i ng-parse
```

```javascript
import { parseFile, parsePath } from 'ng-parse';

const parsedFile = parseFile(filePath, options);
const parsedFiles = parsePath(path, options);
```

## ParsedFile
Both [`parseFile`](#parsefilefilepath-options) and [`parsePath`](#parsepathpath-options) return respectively a `ParsedFile` Object or Array of `ParsedFile` Objects.

* `path`: `string` The file path
* `classes`: [`ParsedClass[]`](#parsedclass) The parsed classes in the file
* `variables`: [`ParsedVariable[]`](#parsedvariable) The parsed variables in the file

### ParsedClass
* `name`: `string` The name of the class
* `methods`: [`ParsedMethod[]`](#parsedmethod) The parsed methods in the class
* `properties`: [`ParsedProperty[]`](#parsedproperty) | [`ParsedSetter[]`](#parsedsetter) | [`ParsedGetter[]`](#parsedgetter) The parsed properties in the class, can exist of plain properties, getters and/or setters
* `setters`: [`ParsedSetter[]`](#parsedsetter) The parsed setters in the class
* `getters`: [`ParsedGetter[]`](#parsedgetter) The parsed getters in the class
* `selector?`: `string` The selector found in the class (optional, elegible for Components and Directives)
* `type?`: `string` The type of the class (optional, mostly the decorator found on the class)

#### ParsedMethod
* `name`: `string` The name of the method
* `type`: `string` The return type of the method (`null` if not present)
* `parameters`: [`ParsedMethodParameter[]`](#parsedmethodparameter) The parameters of the method
* `modifiers`: `ts.SyntaxKind[]` the modifiers of the method

##### ParsedMethodParameter
* `name`: `string` The name of the method parameter
* `type`: `string` The type of the method parameter (`null` if not present)

#### ParsedProperty
* `modifiers`: `ts.SyntaxKind[]` The modifiers of the property
* `name`: `string` The name of the property
* `type`: `string` The type of the property (`null` if not present)
* `initializer`: [`ParsedInitializer`](#parsedinitializer) The initializer of the property (`null` if not present)
* `kind`: `string` The property kind

#### ParsedSetter
* `modifiers`: `ts.SyntaxKind[]` The modifiers of the property
* `name`: `string` The name of the property
* `type`: `string` The type of the property
* `kind`: `string` The property kind
* `parameters`: [`ParsedSetterParameter[]`](#parsedsetterparameter) The parameters of the setter

##### ParsedSetterParameter
* `name`: `string` The name of the setter parameter
* `type`: `string` The type of the setter parameter (`null` if not present)
* `initializer`: [`ParsedInitializer`](#parsedinitializer) The initializer of the setter parameter (`null` if not present)

#### ParsedGetter
* `name`: `string` The name of the getter
* `type`: `string` The type of the getter (`null` if not present)

### ParsedVariable
* `modifiers`: `ts.SyntaxKind[]` The modifiers of the variable
* `initializer`: [`ParsedInitializer`](#parsedinitializer) The initializer of the variable (`null` if not present)
* `name`: `string` The name of the variable
* `type?`: `string` The type of the variable (optional, `null` if not present)
* `propertyName?`: `string` The property name of the variable (optional)

### ParsedInitializer
* `type`: `string` The type of the initializer (`null` if not present)
* `value`: `string` The value of the initializer

## parseFile(filePath, [options])
The `parseFile` method can be used to parse a specific file

* `filePath`: `string` The path to the file, can be absolute or relative (see options)
* `options`: [`ParseFileOptions`](#parsefileoptions) The options to configure the `parseFile` method (optional)

Returns a [`ParsedFile`](#parsedfile) object

### ParseFileOptions
All the options passed to configure the [`parseFile`](#parsefile) method are optional

* `relativePath?`: `string` If filled the passed `filePath` will be merged with the `relativePath`, (optional, default the absolute `filePath` is used)
* `languageVersion?`: `ts.ScriptTarget` The Typescript version of the file (optional, default `Latest`)

## parsePath(path, [options])
The `parsePath` method can be used to parse multiple files by passing a `path` and using a `minimax` file matcher

* `path`: `string` The path from where the matching should start (the `matcher` can be set through the options)
* `options`: [`ParsePathOptions`](#parsepathoptions) The options to configure the `parsePath` method (optional)

Returns an array of [`ParsedFile`](#parsedfile) objects

### ParsePathOptions
All the options passed to configure the [`parsePath`](#parsepath) method are optional

The `ParsePathOptions` type extends from the [`ParseFileOptions`](#parsefileoptions) type

* `relativePath?`: `string` If filled the passed `filePath` will be merged with the `relativePath`, (optional, default the absolute `filePath` is used)
* `languageVersion?`: `ts.ScriptTarget` The Typescript version of the file (optional, default `Latest`)
* `matcher`: `string` The `minimax` matcher used by `glob` to find the files (optional, default `**/*.{ts,tsx}`)

## License

MIT &copy; Jef Van Hoyweghen