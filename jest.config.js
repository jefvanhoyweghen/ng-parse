module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  roots: ['src/'],
  collectCoverageFrom: [
    'src/**/*.ts',
    '!lib/**/*.{ts,js}'
  ],
  coverageReporters: ['json', 'lcov', 'html', 'text', 'text-summary'],
  coverageThreshold: {
    global: {
      // TODO: raise tresholds
      statements: 75,
      branches: 45,
      functions: 80,
      lines: 75
    }
  },
  coverageDirectory: './coverage'
};