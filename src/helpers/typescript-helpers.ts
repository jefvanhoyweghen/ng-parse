import * as ts from 'typescript';
import { ParsedInitializer } from '../../types/parsed-initializer.type';

export function findClassDeclarations(statements: ts.NodeArray<ts.Statement>): ts.Statement[] {
    return statements.filter(statement => ts.isClassDeclaration(statement));
}

export function findVariableStatements(statements: ts.NodeArray<ts.Statement>): ts.Statement[] {
    return statements.filter(statement => ts.isVariableStatement(statement));
}

export function findCallExpression(node: ts.Node): ts.Node | null {
    let callExpression: ts.Node | null;
    callExpression = null;

    node.forEachChild(childNode => {
        if (callExpression === null && ts.isCallExpression(childNode)) {
            callExpression = childNode;
        }
    });

    return callExpression;
}

export function findSelector(callExpression: ts.CallExpression): string {
    return findClassProperty(callExpression, 'selector');
}

export function findClassProperty(callExpression: ts.CallExpression, propertyKey: string): any {
    const properties = findClassProperties(callExpression);

    const initializers = properties
        .filter(property => {
            const name = findIdentifier(property);

            return name.escapedText.toString() === propertyKey;
        })
        .map(property => {
            return property.initializer;
        });

    if (initializers.length) {
        const initializer = initializers[0];

        if (ts.isStringLiteral(initializer)) {
            return initializer.text;
        } else if (ts.isArrayLiteralExpression(initializer)) {
            return initializer.elements;
        } else {
            return initializer;
        }
    }

    return null;
}

export function findClassProperties(callExpression: ts.CallExpression): ts.PropertyAssignment[] {
    return callExpression.arguments.reduce((acc, argument) => {
        if (ts.isObjectLiteralExpression(argument)) {
            const { properties } = argument;

            return [...acc, ...properties];
        }
    }, []);
}

export function findIdentifier(node: ts.Node): ts.Identifier | null {
    let identifier: ts.Node | null;
    identifier = null;

    node.forEachChild(childNode => {
        if (identifier === null && ts.isIdentifier(childNode)) {
            identifier = childNode;
        }
    });

    return identifier as ts.Identifier;
}

export function getIdentifierText(identifier: ts.Identifier): string {
    return identifier.text || identifier.escapedText.toString() || '';
}

export function findProperties(members: ts.NodeArray<ts.ClassElement>): ts.PropertyDeclaration[] {
    return members.filter(member => ts.isPropertyDeclaration(member)) as ts.PropertyDeclaration[];
}

export function findMethods(members: ts.NodeArray<ts.ClassElement>): ts.MethodDeclaration[] {
    return members.filter(member => ts.isMethodDeclaration(member)) as ts.MethodDeclaration[];
}

export function findSetters(members: ts.NodeArray<ts.ClassElement>): ts.SetAccessorDeclaration[] {
    return members.filter(member => ts.isSetAccessor(member)) as ts.SetAccessorDeclaration[];
}

export function findGetters(members: ts.NodeArray<ts.ClassElement>): ts.PropertyDeclaration[] {
    return members.filter(member => ts.isGetAccessor(member)) as ts.PropertyDeclaration[];
}

export function mapInitializer(initializer: ts.Expression): ParsedInitializer {
    let type: string;
    let value: string;

    type = mapKind(ts.SyntaxKind[initializer.kind]) || ts.SyntaxKind[initializer.kind];

    if (ts.isNewExpression(initializer)) {
        value = 'new ' + getText(initializer.expression);
    } else {
        value = getTextOrKind(initializer);
    }

    return {
        type,
        value,
    };
}

export function mapModifier(modifier: string): string {
    const modifierMapper: any = {
        AbstractKeyword: 'abstract',
        AsyncKeyword: 'async',
        ConstKeyword: 'const',
        DeclareKeyword: 'declare',
        DefaultKeyword: 'default',
        ExportKeyword: 'export',
        PrivateKeyword: 'private',
        ProtectedKeyword: 'protected',
        PublicKeyword: 'public',
        ReadonlyKeyword: 'readonly',
        StaticKeyword: 'static',
    };

    return modifierMapper[modifier] || null;
}

export function mapKind(kind: string): string {
    const kindMapper: any = {
        AnyKeyword: 'any',
        ArrayLiteralExpression: 'array',
        BooleanKeyword: 'boolean',
        FalseKeyword: 'false',
        NewExpression: 'new',
        NullKeyword: 'null',
        NumberKeyword: 'number',
        NumericLiteral: 'number',
        ObjectKeyword: 'object',
        StringKeyword: 'string',
        StringLiteral: 'string',
        TrueKeyword: 'true',
        VoidKeyword: 'void',
    };

    return kindMapper[kind] || false;
}

export function exists(property: any): boolean {
    if (property === null) {
        return false;
    }

    if (typeof property === 'undefined') {
        return false;
    }

    return true;
}

export function getText(property: any): string {
    const keys = Object.keys(property);

    if (keys.indexOf('text') > -1 && exists(property.text)) {
        return property.text;
    }

    if (keys.indexOf('escapedText') > -1 && exists(property.escapedText)) {
        return property.escapedText;
    }

    return null;
}

export function getTextOrKind(property: any): string {
    const text = getText(property);

    return text !== null ? text : mapKind(ts.SyntaxKind[property.kind]) || ts.SyntaxKind[property.kind];
}
