import * as ts from 'typescript';
import { ParsedClass } from '../../types/parsed-class.type';
import { ParsedGetter } from '../../types/parsed-getter.type';
import { ParsedInitializer } from '../../types/parsed-initializer.type';
import { ParsedMethod } from '../../types/parsed-method.type';
import { ParsedProperty } from '../../types/parsed-property.type';
import { ParsedSetter, ParsedSetterParameter } from '../../types/parsed-setter.type';
import { ParsedVariable } from '../../types/parsed-variable.type';
import { exists, getText } from './typescript-helpers';
import {
    findCallExpression,
    findGetters,
    findIdentifier,
    findMethods,
    findProperties,
    findSelector,
    findSetters,
    getIdentifierText,
    getTextOrKind,
    mapInitializer,
    mapModifier,
} from './typescript-helpers';

export function parseClassDeclarations(classDeclarations: ts.ClassDeclaration[]): ParsedClass[] {
    return classDeclarations.reduce((acc, classDeclaration) => {
        let name: string = null;
        let type: string = null;
        let selector: string = null;
        let methods: ParsedMethod[] = [];
        let properties: Array<ParsedProperty | ParsedSetter | ParsedGetter> = [];
        let setters: ParsedSetter[] = [];
        let getters: ParsedGetter[] = [];

        const classIdentifier = findIdentifier(classDeclaration);
        name = getIdentifierText(classIdentifier);

        if (classDeclaration.decorators) {
            classDeclaration.decorators.some(decorator => {
                const callExpression = findCallExpression(decorator);
                if (callExpression !== null) {
                    const identifier = findIdentifier(callExpression);
                    if (identifier) {
                        type = getIdentifierText(identifier);
                        selector = findSelector(callExpression as ts.CallExpression);
                        return true;
                    }
                }
            });
        }
        if (classDeclaration.members) {
            const foundProperties = findProperties(classDeclaration.members);
            const foundSetters = findSetters(classDeclaration.members);
            const foundGetters = findGetters(classDeclaration.members);
            const foundMethods = findMethods(classDeclaration.members);

            methods = parseClassMethods(foundMethods);
            properties = parseClassProperties(foundProperties);
            setters = parseClassSetters(foundSetters);
            getters = parseClassGetters(foundGetters);

            const propertyNames = properties.map(property => property.name);
            const setterNames = setters.map(setter => setter.name);

            const uniqueGetterSetters = [...setters, ...getters.filter(getter => setterNames.indexOf(getter.name) < 0)];

            properties = [
                ...properties,
                ...uniqueGetterSetters.filter(getterSetter => propertyNames.indexOf(getterSetter.name) < 0),
            ];
        }

        let parsedClassDeclaration: ParsedClass = {
            getters,
            methods,
            name,
            properties,
            setters,
        };

        if (type !== null) {
            parsedClassDeclaration = {
                ...parsedClassDeclaration,
                type,
            };
        }

        if (selector !== null) {
            parsedClassDeclaration = {
                ...parsedClassDeclaration,
                selector,
            };
        }

        return [...acc, parsedClassDeclaration];
    }, []);
}

export function parseVariableStatements(variableStatements: ts.VariableStatement[]): ParsedVariable[] {
    return variableStatements.reduce((acc, variableStatement) => {
        let modifiers: ts.SyntaxKind[] = [];

        if (variableStatement.modifiers) {
            modifiers = variableStatement.modifiers.reduce((acc, modifier) => {
                return [...acc, mapModifier(ts.SyntaxKind[modifier.kind])];
            }, []);
        }

        const declarations: ParsedVariable[] = variableStatement.declarationList.declarations.reduce(
            (acc, declaration) => {
                let name: string = null;
                let initializer: ParsedInitializer = null;
                let type: string = null;

                name = getText(declaration.name);

                if (exists(declaration.initializer)) {
                    initializer = mapInitializer(declaration.initializer);
                }

                if (exists(declaration.type)) {
                    type = getTextOrKind(declaration.type);
                }

                if (ts.isIdentifier(declaration.name)) {
                    return [
                        ...acc,
                        {
                            initializer,
                            modifiers,
                            name,
                            type,
                        },
                    ];
                } else if (ts.isObjectBindingPattern(declaration.name)) {
                    const nameElements: ParsedVariable[] = declaration.name.elements.reduce((acc, nameElement) => {
                        let name: string = null;
                        let elementInitializer: ParsedInitializer = null;
                        let propertyName: string = null;

                        name = getText(nameElement.name);

                        if (exists(nameElement.initializer)) {
                            elementInitializer = mapInitializer(nameElement.initializer);
                        }

                        if (exists(nameElement.propertyName)) {
                            propertyName = getText(nameElement.propertyName);
                        }

                        return [
                            ...acc,
                            {
                                initializer: elementInitializer !== null ? elementInitializer : initializer,
                                modifiers,
                                name,
                                propertyName,
                            },
                        ];
                    }, []);

                    return [...acc, ...nameElements];
                }
            },
            []
        );

        return [...acc, ...declarations];
    }, []);
}

function parseClassMethods(methods: ts.MethodDeclaration[]): ParsedMethod[] {
    return methods.map(method => {
        let name: string = null;
        let modifiers: any[] = [];
        let type = null;
        let parameters: any[] = [];

        name = getText(method.name);

        if (exists(method.modifiers)) {
            modifiers = method.modifiers.reduce((acc, modifier) => {
                return [...acc, mapModifier(ts.SyntaxKind[modifier.kind])];
            }, []);
        }

        if (exists(method.type)) {
            type = method.type.getText();
        }

        if (exists(method.parameters)) {
            parameters = method.parameters.map(parameter => {
                let type = null;

                if (exists(parameter.type)) {
                    type = parameter.type.getText();
                }

                return {
                    name: parameter.name.getText(),
                    type,
                };
            });
        }

        return {
            modifiers,
            name,
            parameters,
            type,
        };
    });
}

function parseClassProperties(
    properties: ts.PropertyDeclaration[]
): Array<ParsedProperty | ParsedSetter | ParsedGetter> {
    return properties.map(property => {
        let name: string;
        let modifiers: ts.SyntaxKind[] = [];
        let type: string = null;
        let initializer: ParsedInitializer = null;
        let kind: string = null;

        name = getTextOrKind(property.name);

        if (exists(property.modifiers)) {
            modifiers = property.modifiers.reduce((acc, modifier) => {
                return [...acc, mapModifier(ts.SyntaxKind[modifier.kind])];
            }, []);
        }

        if (exists(property.type)) {
            type = getTextOrKind(property.type);
        }

        if (exists(property.initializer)) {
            initializer = mapInitializer(property.initializer);
        }

        if (exists(property.decorators)) {
            const callExpression = findCallExpression(property.decorators[0]);

            if (exists(callExpression)) {
                const identifier = findIdentifier(callExpression);
                if (exists(identifier)) {
                    kind = getIdentifierText(identifier);
                }
            }
        }

        return {
            initializer,
            kind,
            modifiers,
            name,
            type,
        };
    });
}

function parseClassSetters(setters: ts.SetAccessorDeclaration[]): ParsedSetter[] {
    return setters.map(setter => {
        let name: string = null;
        let modifiers: ts.SyntaxKind[] = [];
        let type: string = null;
        let kind: string = null;
        let parameters: ParsedSetterParameter[] = [];

        name = getText(setter.name);

        if (exists(setter.modifiers)) {
            modifiers = setter.modifiers.reduce((acc, modifier) => {
                return [...acc, mapModifier(ts.SyntaxKind[modifier.kind])];
            }, []);
        }

        if (exists(setter.type)) {
            type = getTextOrKind(setter.type);
        }

        if (exists(setter.decorators)) {
            const callExpression = findCallExpression(setter.decorators[0]);

            if (exists(callExpression)) {
                const identifier = findIdentifier(callExpression);
                if (exists(identifier)) {
                    kind = getIdentifierText(identifier);
                }
            }
        }

        if (exists(setter.parameters)) {
            parameters = setter.parameters.map(parameter => {
                let name: string = null;
                let type: string = null;
                let initializer: ParsedInitializer = null;

                name = getText(parameter.name);

                if (exists(parameter.type)) {
                    type = getTextOrKind(parameter.type);
                }

                if (exists(parameter.initializer)) {
                    initializer = mapInitializer(parameter.initializer);
                }

                return {
                    initializer,
                    name,
                    type,
                };
            });
        }

        return {
            kind,
            modifiers,
            name,
            parameters,
            type,
        };
    });
}

function parseClassGetters(getters: ts.PropertyDeclaration[]): ParsedGetter[] {
    return getters.map(getter => {
        let name: string = null;
        let type: string = null;

        name = getTextOrKind(getter.name);

        if (exists(getter.type)) {
            type = getTextOrKind(getter.type);
        }

        return {
            name,
            type,
        };
    });
}
