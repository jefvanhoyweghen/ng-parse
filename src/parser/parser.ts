import * as fs from 'fs';
import * as glob from 'glob';
import * as path from 'path';
import * as ts from 'typescript';
import { ParseFileOptions, ParsePathOptions } from '../../types/parse-options.type';
import { ParsedClass } from '../../types/parsed-class.type';
import { ParsedFile } from '../../types/parsed-file.type';
import { ParsedVariable } from '../../types/parsed-variable.type';
import { parseClassDeclarations, parseVariableStatements } from '../helpers/parser-helpers';
import { findClassDeclarations, findVariableStatements } from '../helpers/typescript-helpers';

export function parseFile(file: string, options: ParseFileOptions = null): ParsedFile {
    let languageVersion = ts.ScriptTarget.Latest;

    if (options !== null) {
        if (options.languageVersion && options.languageVersion in ts.ScriptTarget) {
            languageVersion = options.languageVersion;
        }

        if (options.relativePath) {
            file = path.resolve(options.relativePath, file);
        }
    }

    const sourceCode = fs.readFileSync(file, 'utf8');
    const sourceFile = ts.createSourceFile(file, sourceCode, languageVersion, false);

    const classDeclarations = findClassDeclarations(sourceFile.statements);
    const classes: ParsedClass[] = parseClassDeclarations(classDeclarations as ts.ClassDeclaration[]);

    const variableStatements = findVariableStatements(sourceFile.statements);
    const variables: ParsedVariable[] = parseVariableStatements(variableStatements as ts.VariableStatement[]);

    return {
        classes,
        path: file,
        variables,
    };
}
export function parsePath(pathToParse: string, options: ParsePathOptions = null): ParsedFile[] {
    let matcher = '**/*.{ts,tsx}';

    if (options !== null) {
        if (options.matcher) {
            matcher = options.matcher;
            delete options.matcher;
        }

        if (options.relativePath) {
            pathToParse = path.resolve(options.relativePath, pathToParse);
            delete options.relativePath;
        }
    }

    if (pathToParse.charAt(pathToParse.length - 1) !== '/') {
        pathToParse += '/';
    }

    const foundFiles = glob.sync(matcher, {
        cwd: pathToParse,
        nodir: true,
    });

    return foundFiles.reduce((acc, file) => {
        const inputFile = pathToParse + file;

        return [...acc, parseFile(inputFile, options)];
    }, []);
}
