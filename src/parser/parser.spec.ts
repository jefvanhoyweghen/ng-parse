import * as path from 'path';
import { ParseFileOptions, ParsePathOptions } from '../../types/parse-options.type';
import { ParsedFile } from '../../types/parsed-file.type';
import { parseFile, parsePath } from '../parser/parser';

describe('parser', () => {
    let expectedParsedComponent: ParsedFile;
    let expectedParsedDirective: ParsedFile;
    let expectedParsedModule: ParsedFile;

    beforeEach(() => {
        expectedParsedComponent = {
            classes: [
                {
                    getters: [
                        {
                            name: 'b',
                            type: 'number',
                        },
                    ],
                    methods: [],
                    name: 'TestComponent',
                    properties: [
                        {
                            initializer: null,
                            kind: null,
                            modifiers: ['private'],
                            name: '_b',
                            type: 'number',
                        },
                        {
                            initializer: {
                                type: 'string',
                                value: 'a',
                            },
                            kind: 'Input',
                            modifiers: [],
                            name: 'a',
                            type: 'string',
                        },
                        {
                            kind: 'Input',
                            modifiers: [],
                            name: 'b',
                            parameters: [
                                {
                                    initializer: null,
                                    name: 'b',
                                    type: 'number',
                                },
                            ],
                            type: 'void',
                        },
                    ],
                    selector: 'test-component',
                    setters: [
                        {
                            kind: 'Input',
                            modifiers: [],
                            name: 'b',
                            parameters: [
                                {
                                    initializer: null,
                                    name: 'b',
                                    type: 'number',
                                },
                            ],
                            type: 'void',
                        },
                    ],
                    type: 'Component',
                },
            ],
            path: '',
            variables: [],
        };
        expectedParsedModule = {
            classes: [
                {
                    getters: [],
                    methods: [],
                    name: 'TestModule',
                    properties: [],
                    setters: [],
                    type: 'NgModule',
                },
            ],
            path: '',
            variables: [],
        };
        expectedParsedDirective = {
            classes: [
                {
                    getters: [
                        {
                            name: 'b',
                            type: 'number',
                        },
                    ],
                    methods: [],
                    name: 'TestDirective',
                    properties: [
                        {
                            initializer: null,
                            kind: null,
                            modifiers: ['private'],
                            name: '_b',
                            type: 'number',
                        },
                        {
                            initializer: {
                                type: 'string',
                                value: 'a',
                            },
                            kind: 'Input',
                            modifiers: [],
                            name: 'a',
                            type: 'string',
                        },
                        {
                            initializer: {
                                type: 'new',
                                value: 'new EventEmitter',
                            },
                            kind: 'Output',
                            modifiers: [],
                            name: 'click',
                            type: null,
                        },
                        {
                            kind: 'Input',
                            modifiers: [],
                            name: 'b',
                            parameters: [
                                {
                                    initializer: null,
                                    name: 'b',
                                    type: 'number',
                                },
                            ],
                            type: 'void',
                        },
                    ],
                    selector: 'test-directive',
                    setters: [
                        {
                            kind: 'Input',
                            modifiers: [],
                            name: 'b',
                            parameters: [
                                {
                                    initializer: null,
                                    name: 'b',
                                    type: 'number',
                                },
                            ],
                            type: 'void',
                        },
                    ],
                    type: 'Directive',
                },
            ],
            path: '',
            variables: [],
        };
    });

    describe('given the parseFile method', () => {
        let file: string;
        let expectedPath: string;
        let options: ParseFileOptions;
        let expectedParsedFile: ParsedFile;

        beforeEach(() => {
            options = {
                relativePath: __dirname,
            };
        });

        describe('given a ngModule', () => {
            beforeEach(() => {
                file = '../../test-files/test.module.ts';
                expectedPath = path.resolve(__dirname, file);
                expectedParsedFile = {
                    ...expectedParsedModule,
                    path: expectedPath,
                };
            });

            it('should parse the file correctly', () => {
                const parsedFile = parseFile(file, options);

                expect(parsedFile).toEqual(expectedParsedFile);
            });
        });

        describe('given a Component', () => {
            beforeEach(() => {
                file = '../../test-files/test.component.ts';
                expectedPath = path.resolve(__dirname, file);
                expectedParsedFile = expectedParsedFile = {
                    ...expectedParsedComponent,
                    path: expectedPath,
                };
            });

            it('should parse the file correctly', () => {
                const parsedFile = parseFile(file, options);

                expect(parsedFile).toEqual(expectedParsedFile);
            });
        });

        describe('given a Directive', () => {
            beforeEach(() => {
                file = '../../test-files/test.directive.ts';
                expectedPath = path.resolve(__dirname, file);
                expectedParsedFile = expectedParsedFile = {
                    ...expectedParsedDirective,
                    path: expectedPath,
                };
            });

            it('should parse the file correctly', () => {
                const parsedFile = parseFile(file, options);

                expect(parsedFile).toEqual(expectedParsedFile);
            });
        });
    });

    describe('given the parsePath method', () => {
        let pathToParse: string;
        let expectedPartialPath: string;
        let options: ParsePathOptions;
        let expectedParsedFiles: ParsedFile[];

        beforeEach(() => {
            pathToParse = '../../test-files';
            expectedPartialPath = path.resolve(__dirname, pathToParse);
            expectedParsedFiles = [
                {
                    ...expectedParsedComponent,
                    path: expectedPartialPath + '/test.component.ts',
                },
                {
                    ...expectedParsedDirective,
                    path: expectedPartialPath + '/test.directive.ts',
                },
                {
                    ...expectedParsedModule,
                    path: expectedPartialPath + '/test.module.ts',
                },
            ];
            options = {
                relativePath: __dirname,
            };
        });

        it('should parse all the files correctly', () => {
            const parsedFiles = parsePath(pathToParse, options);

            expect(parsedFiles).toEqual(expectedParsedFiles);
        });
    });
});
