import * as ts from 'typescript';
import { ParsedInitializer } from './parsed-initializer.type';

export interface ParsedVariable {
    modifiers: ts.SyntaxKind[];
    initializer: ParsedInitializer;
    name: string;
    type?: string;
    propertyName?: string;
};
