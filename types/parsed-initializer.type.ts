export interface ParsedInitializer {
    type: string;
    value: string;
};