import { ParsedGetter } from './parsed-getter.type';
import { ParsedMethod } from './parsed-method.type';
import { ParsedProperty } from './parsed-property.type';
import { ParsedSetter } from './parsed-setter.type';

export interface ParsedClass {
    name: string;
    methods: ParsedMethod[];
    properties: (ParsedProperty | ParsedSetter | ParsedGetter)[];
    setters: ParsedSetter[];
    getters: ParsedGetter[];
    selector?: string;
    type?: string;
};
