import * as ts from 'typescript';
import { ParsedInitializer } from './parsed-initializer.type';

export interface ParsedSetter {
    modifiers: ts.SyntaxKind[];
    name: string;
    type: string;
    kind: string;
    parameters: ParsedSetterParameter[];
};

export interface ParsedSetterParameter {
    name: string;
    type: string;
    initializer: ParsedInitializer;
};
