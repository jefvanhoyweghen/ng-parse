import { ParsedClass } from './parsed-class.type';
import { ParsedVariable } from './parsed-variable.type';

export interface ParsedFile {
    path: string;
    classes: ParsedClass[];
    variables: ParsedVariable[];
};
