import * as ts from 'typescript';

export interface ParseFileOptions {
    relativePath?: string;
    languageVersion?: ts.ScriptTarget;
};

export interface ParsePathOptions extends ParseFileOptions {
    matcher?: string;
};
