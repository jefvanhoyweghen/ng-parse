import * as ts from 'typescript';
import { ParsedInitializer } from './parsed-initializer.type';

export interface ParsedProperty {
    modifiers: ts.SyntaxKind[];
    name: string;
    type: string;
    initializer: ParsedInitializer;
    kind: string;
};
