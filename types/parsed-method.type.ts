import * as ts from 'typescript';

export interface ParsedMethod {
    name: string;
    type: string;
    parameters: ParsedMethodParameter[];
    modifiers: ts.SyntaxKind[];
};

export interface ParsedMethodParameter {
    name: string;
    type: string;
};
