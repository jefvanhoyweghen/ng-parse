@Directive({
    selector: 'test-directive'
})
class TestDirective {
    private _b: number;

    @Input()
    a: string = 'a';

    @Input('b')
    set b(b: number): void {
        this._b = b;
    }
    get b(): number {
        return this._b;
    }

    @Output()
    click = new EventEmitter();
}