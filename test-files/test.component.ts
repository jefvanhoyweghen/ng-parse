@Component({
    selector: 'test-component',
    template: `
        A: {{ a }}
        B: {{ b }}
        <a onclick="onClick()">Click</a>
    `
})
export class TestComponent {
    private _b: number;

    @Input()
    a: string = 'a';

    @Input('b')
    set b(b: number): void {
        this._b = b;
    }

    get b(): number {
        return this._b;
    }
}